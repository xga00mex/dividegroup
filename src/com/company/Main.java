package com.company;
import java.util.*;
public class Main {

    public static void main(String[] args) {
        System.out.println("請輸入需要隨機分組的人數");
	    Scanner in = new Scanner(System.in);
        int num = in.nextInt();//總共多少人
        System.out.println("請輸入一組的人數");
        Scanner in2 = new Scanner(System.in);
	    int team = in2.nextInt();//一組多少人
	    int fullteam; //幾組是滿的
	    int nfullteam;//幾組不是滿的
	    List<Integer> allpeople = new ArrayList<Integer>(num);
        for(int i = 0; i<num;i++){//將人填滿
            allpeople.add(i+1);
        }
        for(int i = 0;i<num;i++){//將人序打亂
            Collections.swap(allpeople,i,(int) (Math.random() * allpeople.size() - 1));
        }
        switch (num%team){//有幾組是滿的 有幾組是少一人的
            case 0:
                fullteam = num/team;
                nfullteam = 0;
                break;
            default:
                fullteam = (int)num/team - team + num%team + 1;
                nfullteam = team - num%team;
                break;
        }
        for(int i = 0;i<fullteam;i++){
            System.out.println("第"+(i+1)+"組");
            for(int j = 0;j<team;j++){
                System.out.print(allpeople.get(j+i*team)+"\t");
            }
            System.out.println();
        }
       for(int i = fullteam;i<fullteam+nfullteam;i++){
            System.out.println("第"+(i+1)+"組");
            for(int j = 0;j<team-1;j++){
                System.out.print(allpeople.get(j+fullteam*team+(i-fullteam)*(team-1))+"\t");//從fullteam結束後開始
            }
           System.out.println();
        }
    }
}
